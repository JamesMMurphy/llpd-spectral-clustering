Longest Leg Path Distance (LLPD) Algorithm

(c) Anna Little and James M. Murphy

Branch v2.1 holds the latest stable version of the algorithm

This directory implements longest leg path distance (LLPD) spectral 
clustering.  The method uses a blackbox eigenesolver based on a learned 
hierarchical decomposition of the graph Laplacian.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To see a demonstration, run one of the scripts inside
LLPD_Code/LLPD_SpectralClustering/ExperimentScripts. The version v2.0
code release includes demo scripts for 4 synthetic and 5 real data sets.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To quickly perform LLPD based spectral clustering using default settings
given an n by d data matrix X of n points in R^d, use the
LLPD_SpectralClustering function inside LLPD_Code/LLPD_SpectralClustering

Example (limited output, default settings): 
[Labels, K_EstimateLLPD] = LLPD_SpectralClustering(X,[],[],[])

Additional output may be requested, and optional input structures may be
given, so that the more general form is given by:

Example (all output, custom settings):
[Labels, K_EstimateLLPD, Sigma_EstimateLLPD, Sigma_LLPD, EigVals_LLPD,
EigVecs_LLPD] = LLPD_SpectralClustering(X, LLPDopts, DenoisingOpts,
SpectralOpts)

See the documentation below for details on the options specified by the
LLPDopts, DenoisingOpts, and SpectralOpts structures.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

If a ground truth file is available or to make comparisons with K-means
or Euclidean spectral clustering, run the GeneralScript_LLPD_SC.m script as
follows:

1. Save an n by d matrix of n data points in R^d as variable X in workspace, as well as an n by 1 vector of ground truth labels as LabelsGT if accuracy quantification relative to ground truth is desired.

2. Navigate to the LLPD_Code/LLPD_SpectralClustering folder.

3. Run addpath(genpath('../../LLPD_Code')) to add all LLPD_Code subfolders to Matlab search path.

4. Run SetDefaultParameters.m to set default parameter values. This creates the
LLPDopts, DenoisingOpts, SpectralOpts, and ComparisonOpts structures
which are required to run GeneralScript_LLPD_SC.m 

5. Optional: adjust parameters. The primary parameters that may need
adjusting are the range of sigma values for LLPD spectral clustering. This can be adjusted using by setting SpectralOpts.SigmaScaling = 'Manual' and specifying the desired values in SpectralOpts.SigmaValues. 

6. Run GeneralScript_LLPD_SC.m.

See one of the demo scripts illustrating the above procedure. See the documentation below for details on the options specified by the
LLPDopts, DenoisingOpts, SpectralOpts, and ComparisonOpts structures.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Structures defining LLPD spectral clustering options:

LLPDopts
    LLPDopts.ThresholdMethod: 'LogScaling' or 'PercentileScaling' (method for computing sequence of scales used for LLPD approximation; default: 'LogScaling')
    LLPDopts.UseFixedNumScales: 0 or 1 (if 0, number of scales is determined by LLPDopts.PercentileSize or LLPDopts.LogRatio; if 1, number of scales is fixed by LLPDopts.NumScales; default: 1)
    LLPDopts.NumScales: number of scales to use in LLPD approximation (high values yield low LLPD approximation error; default: 20)
    LLPDopts.FullyConnected: 0 or 1 (if 1, when KNN graph is disconnected the algorithm adds small edges until the graph becomes connected; default: 1; LLPDopts.FullyConnected=0 is not recommended) 
    LLPDopts.KNN: integer specifying number of NN's for the KNN graph construction used to approximate LLPD (default:20)
    LLPDopts.NumberofSamplePoints = integer specifying how many points to sample from each connected components when determining which small edges to add to a disconnected KNN graph (required if LLPDopts.FullyConnected=1; default: 10)

DenoisingOpts
    DenoisingOpts.KNN: number of NN's to use during denoising (denoising is based on LLPD to DenoisingOpts.KNN-th NN; default: 20) 
    DenoisingOpts.Method: 'Cutoff', 'ExamineFigure', or 'None' ('Cutoff' removes all points whose LLPD to their DenoisingOpts.KNN nearest neighbor exceeds the cutoff specified in DenoisingOpts.Cutoff; 'Examine Figure' allows user to select the cut-off after examining a plot of KNN LLPD's; 'None' keeps all data points; default: 'ExamineFigure')  
    DenoisingOpts.Cutoff: denoising cutoff (required if DenoisingOpts.Method='Cutoff'; algorithm removes all points whose LLPD to their DenoisingOpts.KNN nearest neighbor exceeds this cutoff)
        
SpectralOpts
    SpectralOpts.NumEig: integer number of Laplacian eigenvalues and eigenvectors to compute (default: 30)
    SpectralOpts.Laplacian: 'Symmetric_Laplacian' or 'RandomWalk_Laplacian' (Laplacian to use for LLPD spectral clustering; default: 'Symmetric_Laplacian')
    SpectralOpts.RowNormalization: 0 or 1 (if 1, normalizes rows of eigenvector matrix before clustering; default: 0)
    SpectralOpts.SigmaScaling: 'Automatic' or 'Manual' (method for computing sigma scaling for LLPD SC; default: 'Automatic')
    SpectralOpts.NumReplicates: integer specifying number of Kmeans replicates to use for clustering the LLPD spectral embedding (default: 5)        

ComparisonOpts
    ComparisonOpts.RunEucSC: 0 or 1 (if 1, algorithm returns Euclidean spectral clustering results; default: 0)
    ComparisonOpts.RunKmeans: 0 or 1 (if 1, algorithm returns Kmeans clustering results; default: 1)
    ComparisonOpts.EucSCNumEig: integer specifying how many Laplacian eigenvalues and eigenvectors to compute (default: 30)
    ComparisonOpts.EucSCSigmaScaling: 'Automatic' or 'Manual' (method for computing sigma scaling for Euclidean SC; default: 'Automatic')
    ComparisonOpts.EucSCNumScales: integer specifying how many scales to use when computing Euclidean SC (default: 20; required when ComparisonOpts.EucSCSigmaScaling='Automatic')
    ComparisonOpts.EucSCSigmaValues: vector of sigma values to use when computing Euclidean SC (required when ComparisonOpts.EucSCSigmaScaling='Manual')
    ComparisonOpts.EucSCSparsity: 'None' or 'KNN' ('None' uses dense Laplacian, 'KNN' uses sparse Laplacian constructed from LLPDopts.KNN distances only; default: 'None') 
    ComparisonOpts.EucSCNumReplicates: integer specifying number of Kmeans replicates to use for clustering the Euclidean spectral embedding (default: 5)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


If this code is used in any publication, please cite the following paper: 

Little A, Maggioni M, Murphy JM. 
Path-Based Spectral Clustering: Guarantees, Robustness to Outliers, and Fast Algorithms.
arXiv preprint arXiv:1712.06206. 
2017 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.